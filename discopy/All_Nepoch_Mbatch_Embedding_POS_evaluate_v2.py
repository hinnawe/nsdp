from keras.models import load_model
import pickle_save
import numpy as np

import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]= "1"
from keras import backend as K
K.tensorflow_backend._get_available_gpus()

max_len = 1169
max_len_char = 54

X_test   = pickle_save.read_pickle("X_test_pre")
P_test   = pickle_save.read_pickle("P_test_pre")
X_index  = pickle_save.read_pickle("X_index_pre")
X_char_test = pickle_save.read_pickle("X_char_list_test")
tags = pickle_save.read_pickle("tag2idx")
indices = pickle_save.read_pickle("idx2tag")

X_char_test = np.array(X_char_test).reshape(len(X_char_test),max_len,max_len_char)

model = load_model('neural_models/15epoch_64batch_00_emb_pos_pre_drop5')
output_relations = []

for i in range(0,len(X_test)):
    index = X_index[i]
    doc = index[0][0]
    p = model.predict( [np.array([X_test[i]]),np.array([P_test[i]])] )
    p = np.argmax(p, axis=-1)
    predict_list = list(p[0])
    predict_tag_list = []
    for item in predict_list:
        predict_tag_list.append(indices[item])
    tag_index = 0
    relation_dict = {'Arg1': {'TokenList': []}, 'Arg2': {'TokenList': []}, 'Connective': {'TokenList': []}}
    for tag in predict_tag_list:
        if tag == 'arg1':
            relation_dict['Arg1']['TokenList'].append(index[tag_index][1])
            tag_index +=1
        elif tag == 'arg2':
            relation_dict['Arg2']['TokenList'].append(index[tag_index][1])
            tag_index +=1
        elif tag == 'connective':
            relation_dict['Connective']['TokenList'].append(index[tag_index][1])
            tag_index +=1
        elif tag == 'none':
            tag_index +=1
    relation_dict['DocID'] = doc
    relation_dict['Sense'] = ['Expansion.Conjunction'] # in order to comply with the conll evaluator, we add this dummy sense
    relation_dict['Type'] = 'Explicit'
    output_relations.append(relation_dict)
    print(i+1, "out of 996 relations processed")

import json
with open("json_files/15epoch_64batch_00_Embedding_POS_pre_drop5.json", "w") as f:  # open the desired output file with the correct model name
    if output_relations:  # a check that our array is not empty
        for dictionary in output_relations:  # loop through dictionaries one by one
            json.dump(dictionary, f)  # JSON encode each element and write it as a line in the file
            if output_relations.index(dictionary) != len(output_relations) -1: # if the dictionary is not the last item in list
                f.write("\n")  # add a new line after each dict 
    f.truncate()  # remove the rest, just in case    
