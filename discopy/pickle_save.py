import pickle
def write_pickle( Variable, fname):
    filename = fname +".pkl"
    f = open("./pickle_vars/"+filename, 'wb')
    pickle.dump(Variable, f)
    f.close()
def read_pickle(fname):
    filename = "./pickle_vars/"+fname +".pkl"
    f = open(filename, 'rb')
    obj = pickle.load(f)
    f.close()
    return obj