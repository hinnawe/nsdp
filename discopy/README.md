NAL for SDP: Guidelines
October 2, 2019

Here we provide some guidance for the repository:

1. data processing.ipynb: This is the notebook where we handle all the
data preprocessing:
(a) We read the training and development sets and write them in csv
les in the desired format.
(b) We do the same for the test set, except that the csv le contains only
None labels instead of true labels.
(c) We read the training and development csv les, combine them in
a dataframe. Then we process it to get information such as the
maximum length of the relation spans, number of unique words etc.
to use them later in the neural network. We record all these variables
that contain information to be used in the network as pickle les
under the pickle vars folder (see the 2nd point below).
(d) Using the generated variables, we transform the raw data into a for-
mat required by the neural network model. We do that for the POS
input and character embeddings separately.
(e) There are certain portions in the le that has been used for analyzing
the best window size, or constructing the model and the evaluation
script we used earlier. All those are taken care of in other scripts that
are described later in this document, so please ignore them. All the
actual information generated in this le that was used in the training
and evaluation processes is stored in pickle les.
2. pickle vars folder: This is the folder where we collect all the variables
and stored information that is used in dierent phases of the project. It
mainly consists of:
(a) The history logs of the models that we trained
(b) The word embedding matrix versions
(c) Dictionaries that match a token, a POS tag etc. to their index values
that represent them in the neural network.
1
(d) Prepadded or postpadded versions of the training, development and
test sets that are transformed into a neural network compatible for-
mat.
3. csv les folder: This is basically the folder that contains the csv versions
of the datasets.
4. model emb pos.py script: This is the baseline model construction script
for our model that consists of word embedding and the POS tags. Pre-
viously generated pickle variables are used here. By changing certain
parameters we built dierent models.
5. model emb pos cnn.py script: This is the same as above, now also
including the character embedding component.
6. neural models folder: This is the folder where we save dierent versions
of models. The names of saved models give clues about the components
of the model as well as certain hyperparameters. Since we have a lot of
models collected here (and actually each of us have other models stored in
their own directories), we provide here the names of the four best models
we had, as described in the project report:
(a) 15epoch 64batch 00 emb pos post drop5
(b) 15epoch 64batch 00 emb pos pre drop5
(c) 15epoch 64batch 00 emb pos cnn pre chardrop0 lters100
(d) 15epoch 64batch 00 emb pos cnn pre chardrop0
7. All Nepoch Mbatch Embedding POS evaluate v2.py script: This
is the script that reads the pickle les that store information about the
generated test relations, and yields an output json le containing all the
predictions in a format desired by the ocial conll evaluator (see the 10th
point for that). Please note that this version of the script is designed
for the POS + CNN model evaluation, so for the POS-only version there
should be minor modications. The output json les are stored under the
folder json les with matching names.
8. json les folder: Contains all the predicted output les. Please use the
json les in this folder while using the evaluation script described under
the 10th bullet.
9. parser models and parser jsons folders: These two contain the mod-
els and the outputs that we had while using the Lin parser in the git
depository. They're named to reveal how many epochs were used in train-
ing. The best performing output version ('500 epochs test.json') is used
in data processing.ipynb while creating the test relations with appro-
priate window sizes.
2
10. conll16st folder: It is retrieved from https://github.com/attapol/conll16st
(i.e. the ocial evaluator that is also used by others). Inside there is a scorer.py
script that is used in all our evaluations. The F1 scores we mentioned in
our project report are generated using this script and the output json les
mentioned in bullet number 8. The instructions to use the scorer.py script
are given in the github page.
11. Finally in the repository there are other scripts that are partially used in
other main scripts (such as a pickle saving script).
3