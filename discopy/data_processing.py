#!/usr/bin/env python
# coding: utf-8

# ## Data Preprocessing

# #### Load required libraries

# In[1]:


from __future__ import print_function
import json
import nltk
import os,sys
import codecs
import nltk
from nltk import word_tokenize
import re
import csv
import numpy as np
import pandas as pd
import pickle
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical

#from discopy_new.discopy.discopy.conn_head_mapper import ConnHeadMapper
#from discopy_new.discopy.discopy.argument_extract import add_connective_heads


# #### List all the corresponding paths for the pdtb datasets we have at hand 

# In[2]:


train_relations_path = './data/en.train/relations.json'
train_parses_path = './data/en.train/parses.json'
dev_relations_path = './data/en.dev/relations.json'
dev_parses_path = './data/en.dev/parses.json'
train_conll_format_path = './data/en.train/conll_format'
dev_conll_format_path = './data/en.dev/conll_format'
train_raw_path = './data/en.train/raw'
dev_raw_path = './data/en.dev/raw'


# #### Read the json files for training & development datasets (both relations and parses) and store them in varibles 

# In[3]:


train_relations = [json.loads(s) for s in open(train_relations_path, 'r').readlines()]
train_parses = json.loads(open(train_parses_path).read()) 
dev_relations = [json.loads(s) for s in open(dev_relations_path, 'r').readlines()]
dev_parses = json.loads(open(dev_parses_path).read())


# As an example, the training relations are in the following format:

# In[4]:


train_relations[97]


# #### Basically the relations consist of the following information: <br>
# The document id ('DocID') <br>
# The sentence id ('ID')<br>
# The sense of the connective ('Sense')<br>
# The type of the relation ('Type')<br>
# For both **arg1** and **arg2**, and the **connective**: <br>
# - The indices of the first and last character of the whole raw text within that document ('CharacterSpanList')
# - The raw text ('RawText')
# - A list of lists, where each sublist consists of the following five elements _for each token_ in the sentence  ('TokenList'):
# > - The first character index within the document
# > - The last character index within the document
# > - The index of the token within the document (0 is the very first token in that very document)
# > - The index of the sentence within the document
# > - The index of the token within the sentence
# 
# Remember that for the **connective**, there is no information when the type of the relation is **non-explicit**. <br>
# 

# #### From this information, using the following function, we extract certain required portions to form a summary of relations

# In[5]:


def generate_dataset_token(pdtb):
    outlist=[]
    #add_connective_heads(pdtb)
    doc_before = ""
    
    count_total = 0 
    count_arg_1=0
    for relation in filter(lambda i: i['Type'] == 'Explicit', pdtb):
        
        doc_id = relation['DocID']
        if doc_before != doc_id:
            count=0
                
        # get the sentence indices for arg1 & arg2 & conn
        arg1_sentences = set([sent_ind 
                                for char_start,char_end,token_ind, 
                                    sent_ind, token_ind_sent 
                                in relation['Arg1']['TokenList']])        
        arg2_sentences = set([sent_ind 
                                for char_start,char_end,token_ind, 
                                    sent_ind, token_ind_sent 
                                in relation['Arg2']['TokenList']])
        conn_sentences = set([sent_ind 
                                for char_start,char_end,token_ind, 
                                    sent_ind, token_ind_sent 
                                in relation['Connective']['TokenList']])
        
        arg_set = arg1_sentences.union(arg2_sentences) # comb of arg1 and arg2
        cond_connective_in_same_set = len(conn_sentences&arg_set) != len(conn_sentences)
        

        Arg1_list = [[sent_ind, token_ind_sent] for char_start,char_end,token_ind, sent_ind, token_ind_sent 
                 in relation['Arg1']['TokenList']]
        Arg2_list = [[sent_ind, token_ind_sent] for char_start,char_end,token_ind, sent_ind, token_ind_sent 
                 in relation['Arg2']['TokenList']]
        con_list = relation['Connective']['TokenList'][0][3:5]
        
        sent_list = list(set(list(arg1_sentences)+list(arg2_sentences)+list(conn_sentences))) # list of sent indices
                                                                                              # that include everything
        maxim, minim = max(sent_list), min(sent_list)
        total_range = list(range(minim,maxim+1)) # now includes also the sentences that lie in between
        
        outlist.append([doc_id,count,Arg1_list,Arg2_list,con_list,total_range])
        
        count=count+1       
        count_arg_1 += 1        
        count_total += 1
        doc_before = doc_id
    
    print("A total of", count_total, "relations are processed &" , count_arg_1, "arg_1's are found")      
        
    return outlist


# In[6]:


generate_dataset_token(dev_relations)[0:1]


# In[7]:


train_spans = generate_dataset_token(train_relations)
dev_spans = generate_dataset_token(dev_relations)


# #### So the format that we have at hand right now is like this: <br>
# A list of lists, where each sublist corresponds to an **explicit** relation and includes:
# - Doc_ID that this relation belongs to
# - Number of the relation that is processed within this Doc
# - A list of lists where each sublist includes the sentence id and the token id in that sentence, for arg1
# - Same for arg2
# - Same for the connective
# - The list of sentence indices that contain the whole relation: (e.g. if arg1 and conn is in the first sentence, and arg2 is in the third sentence, than this list will be [1,2,3]

# ---------
# ---------

# #### Now we have an exhaustive list of all the explicit relations with their corresponding Doc_IDs. We know which sentences correspond to arguments, also which tokens in those sentences are really the part of the argument, and if the arguments are far apart, what are the sentences lying in between. If we could get the token of all documents in an ordered list, it would be possible to use the output of the previous function to actually tag all the tokens within a relation as arg1, arg2, conn or none:

# In[8]:


def readPathToken(path):
    dictWords= {}
    dictPOS  = {}
    dictIndex = {}
    files = os.listdir(path) #returns a list containing the names of the entries in the directory given by path
    final_list = []
    for name in files: # each name will be in this example format: wsj_2216.conll
        full_path = os.path.join(path, name) # we get something like:'./data/conll2016/en.dev/conll_format/wsj_2216.conll'
        sentence_list = []
        pos_list = []
        index_list = []
        with codecs.open(full_path,'r',encoding='utf-8',errors='ignore') as fdata:
            filedata = fdata.read()
        
        span_list  = filedata.split('\n\n') # spanlists will have a format like: 
                        # ['0\t0\t0\tBancOklahoma\tNNP\t_\t_\t_\n1\t0\t1\tCorp.\tNNP\t_\t_\t_\n2\t0\t2\tsaid\tVBD\t_\.......]
        for span in span_list[:-1]: # don't take the very last element, because it is ''
            wordList=[]
            posList =[]
            indexList = []
            
            #split with new line
            span_lines = span.split('\n')
            for word_tab in span_lines:
                words = word_tab.split('\t') # so after some processing, the format of the words will be like:
                                             # ['0', '0', '0', 'BancOklahoma', 'NNP', '_', '_', '_']
                try:
                    wordList.append(words[3]) # so the 4th item includes the token itself
                    posList.append(words[4]) # and the 5th item is its POS
                    indexList.append(words[0]) # the first item is its index in the document
                except:
                    print("error at : ", full_path)
                    
            sentence_list.append(wordList) # for each span in the span list, we collect the words and the store them in a list
            pos_list.append(posList) # the same is valid for the POS tags
            index_list.append(indexList)
            
        name = name[:-6] # remove the '.conll' part of the name   
        dictWords[name] = sentence_list
        dictPOS[name] = pos_list
        dictIndex[name] = index_list
    return [dictWords , dictPOS, dictIndex]


# #### The previous function returns two dictionaries that are keyed by Doc_ID's, and valued by a list of lists where each sublist includes either the tokens of ordered sentences in that Doc, or the POS tags. After recording these dictionaries for the training and development sets, an example is given:

# In[9]:


train_words, train_POS, train_index = readPathToken(train_conll_format_path)
dev_words, dev_POS, dev_index = readPathToken(dev_conll_format_path)


# In[10]:


dev_words


# #### It is time to merge these functions and create a final output csv file that holds every information that we seek:

# First a handler function for writing csv's.

# In[11]:


def writeCSV(filename, outlist):    
    myFile = open(filename, 'w', newline='')
    with myFile:
        writer = csv.writer(myFile)
        writer.writerows(outlist)
    print("Writing complete")


# Then the main function for merging information is as follows:

# In[12]:


def makeCSV(output_csv_filename, span, dict_sent, dict_POS):
    
    finalList = []
    for doc_Id, span_Id, arg1, arg2, connector, combined in span:
        words_comb = {}
        pos_comb = {}
        span_list = {}
        span_total_list = []
        for sentence_number in combined:
            words_comb[sentence_number]  = dict_sent[doc_Id][sentence_number] # retrieves the words of the nth sentence from that doc
            pos_comb[sentence_number]  = dict_POS[doc_Id][sentence_number]  # the same for POS tags
            #make data structure for holding values
            span_list[sentence_number] = [[doc_Id, span_Id, word, pos, ""]                                           for word, pos in zip(words_comb[sentence_number],pos_comb[sentence_number])] 

        #arg1
        for sentID, tokenID in arg1:
            span_list[sentID][tokenID][4]= 'arg1'
        #arg2
        for sentID, tokenID in arg2:
            span_list[sentID][tokenID][4]= 'arg2'
        #connective
        a_np = np.array(connector)
        if len(a_np.shape)>1:
            for sentID, tokenID in connector:
                span_list[sentID][tokenID][4]= 'connective'
        else:
            span_list[connector[0]][connector[1]][4]= 'connective'   
        #none for rest
        for key, valList in span_list.items():
            for count in range(0,len(valList)):
                val = span_list[key][count][4]
                if val == '':
                    span_list[key][count][4]='none'

        #Making span total list
        for key, valList in span_list.items():
            span_total_list = span_total_list+valList
        #final list
        finalList= finalList+span_total_list

    writeCSV(output_csv_filename, finalList)


# In[13]:


makeCSV("csv_files/dev_token_all.csv", dev_spans ,dev_words, dev_POS )
makeCSV("csv_files/train_token_all.csv", train_spans ,train_words, train_POS)


# # For the test part:
# -----

# In[14]:


def test_connectives(pdtb, sent_before, sent_after):
    outlist=[]
    doc_before = ""
    
    count_total = 0 
    count_arg_1=0
    for relation in filter(lambda i: i['Type'] == 'Explicit', pdtb):
        
        doc_id = relation['DocID']
        if doc_before != doc_id:
            count=0
                
        # get the sentence indices for conn
        conn_sentences = list(set([sent_ind 
                                for char_start,char_end,token_ind, 
                                    sent_ind, token_ind_sent 
                                in relation['Connective']['TokenList']]))
        
        # specify the window size
        if conn_sentences[0] - sent_before < 0: # if n sentences before the connective goes below 0, we need to restrict it with 0
            arg_first = 0
            arg_last = conn_sentences[0] + sent_after
            arg_span = list(range(arg_first, arg_last + 1))
        else:
            arg_first = conn_sentences[0] - sent_before
            arg_last = conn_sentences[0] + sent_after
            arg_span = list(range(arg_first, arg_last + 1))

        con_list = relation['Connective']['TokenList'][0][3:5] # [sent index in the doc, connective token index in the sent]
        
        outlist.append([doc_id, count, arg_first, arg_last, con_list, arg_span])
        
        count = count + 1       
        count_arg_1 += 1        
        count_total += 1
        doc_before = doc_id
    
    print("A total of", count_total, "relations are processed &" , count_arg_1, "arg_1's are found")      
        
    return outlist


# In[15]:


test_spans = test_connectives([json.loads(s) for s in open('parser_jsons/500_epochs_test.json', 'r').readlines()],0,0)


# In[16]:


test_spans[0]


# In[17]:


test_words, test_POS, test_index = readPathToken('./data/en.test/conll_format')


# In[18]:


test_words


# In[19]:


def makeCSV_test(output_csv_filename, span, words, POS, indices):
    
    finalList = []
    final_out_list = [] # same placeholder, but will include also the indices of tokens
    
    for doc_Id, span_Id, arg1, arg2, connector, combined in span: 
        words_comb = {}
        pos_comb = {}
        indices_comb = {} # dictionary of indices
        
        span_list = {}
        output_span_list = {}
        
        span_total_list = []
        span_out_total_list = []
        # we have to make sure that the relation sentence span is within the bounds of the document
        # if not, we need to limit the span with the final sentence index of the document
        first_sent_index = combined[0]
        for sentence_number in combined:            
            if sentence_number >= len(words[doc_Id]):
                combined = list(range(first_sent_index, len(words[doc_Id])))
                
        for sentence_number in combined: # for all the sentences in the relation span
            words_comb[sentence_number]  = words[doc_Id][sentence_number] # retrieves the words of the nth sentence from that doc
            pos_comb[sentence_number]  = POS[doc_Id][sentence_number]  # the same for POS tags
            #make data structure for holding values
            span_list[sentence_number] = [[doc_Id, span_Id, word, pos, "none"]                                           for word, pos in zip(words_comb[sentence_number],pos_comb[sentence_number])]   

            indices_comb[sentence_number] = indices[doc_Id][sentence_number]  # the same for sentence indices
            output_span_list[sentence_number] = [[doc_Id, span_Id, word, pos, "none", index]                                           for word, pos, index in zip(words_comb[sentence_number],                                                                        pos_comb[sentence_number], indices_comb[sentence_number])]
            
        #Making span total list
        for key, valList in span_list.items():
            span_total_list = span_total_list + valList
        for key, valList in output_span_list.items():
            span_out_total_list = span_out_total_list + valList
        #final lists
        finalList = finalList + span_total_list
        final_out_list = final_out_list + span_out_total_list

    writeCSV(output_csv_filename, finalList)
    return final_out_list


# In[20]:


test_data = makeCSV_test("csv_files/test_token_all.csv",test_spans,test_words,test_POS,test_index)


# In[21]:


test_data


# In[22]:


import pandas as pd 
data = pd.read_csv("csv_files/test_token_all.csv", header=None) 
doc_list = data[0].unique()
count = 0
for doc in doc_list:
    sub_data = data[data[0]==str(doc)]
    relation_counts = sub_data[1].unique()
    max_count = max(relation_counts) + 1
    count += max_count
print("There are", count, "relations recorded in the csv file!")


# In[23]:


conn_to_arg1 = [] # a list for storing the distance from the sentence containing the connective, to the sentence containing arg1
conn_to_arg2 = [] # the same for arg2
for span in train_spans:
    connective = span[4][0] #sentence index of the connective in a given relation span
    arg1 = span[2] # arg1 info
    arg2 = span[3] # arg2 info
    arg1_sent_list = []
    arg2_sent_list = []
    for token in arg1:
        arg1_sent_list.append(token[0]) # store the sentence indices of arg1s in a list
    arg1_list = list(set(arg1_sent_list)) # remove duplicates from that list
    arg1_min = min(arg1_list) # find the minimum sentence index
    arg1_max = max(arg1_list) # and the max
    
    for token in arg2: # same procedure for arg2 again
        arg2_sent_list.append(token[0])
    arg2_list = list(set(arg2_sent_list))
    arg2_min = min(arg2_list)
    arg2_max = max(arg2_list)
    
    # now find the greatest distance from the connective, to arg1 and arg2 respectively. 
    # this procedure assumes that most of the time arg1 will come before the connective, and arg2 after the connective
    conn_to_arg1.append(max(connective - arg1_min, connective - arg1_max)) 
    conn_to_arg2.append(max(arg2_min - connective, arg2_max - connective))

        
print("On average, the arg1 comes", sum(conn_to_arg1)/len(train_spans),       "sentences before the sentence that contains the connective")
print("On average, the arg2 comes", sum(conn_to_arg2)/len(train_spans),       "sentences before the sentence that contains the connective")


# ---------------
# ---------------

# #### We need to find all the unique words in the whole dataset (training and development combined) to create word embeddings:

# In[ ]:


# read the csv files generated
data_train = pd.read_csv("csv_files/train_token_all.csv",header=None)
data_dev   = pd.read_csv("csv_files/dev_token_all.csv" ,header=None)
# add column names
data_train.columns=['file','spanNo','word','pos','tag']
data_dev.columns=['file','spanNo','word','pos','tag']
# add another column of lowercased tokens
data_train['lower_word'] = data_train['word'].str.lower()
data_dev['lower_word']   = data_dev['word'].str.lower()
# and combine them 
data_combined = data_train.append(data_dev)


# The combined data should look like:

# In[ ]:


data_combined[0:3]


# In[ ]:


# get the uniqe words and add 'endpad' for padding operation
unique_words = list(set(data_combined["lower_word"].values))
unique_words.append("endpad")
# count the number of unique words
unique_word_count = len(unique_words)
print("Number of total unique words:", unique_word_count)


# Do the same for the relation tags and the POS tags respectively

# In[ ]:


unique_tags = list(set(data_combined["tag"].values))
unique_tags.append("endpad")
unique_tag_count = len(unique_tags)
print("Number of total unique tags:", unique_tag_count)


# In[ ]:


unique_POS = list(set(data_combined["pos"].values))
unique_POS.append("endpad")
unique_POS_count = len(unique_POS)
print("Number of total unique POS tags:", unique_POS_count)


# #### We use pickle to record the variables we generated. They will be used later in network training

# In[ ]:


def writePickle( Variable, fname):
    filename = fname +".pkl"
    f = open("pickle_vars/"+filename, 'wb')
    pickle.dump(Variable, f)
    f.close()
def readPickle(fname):
    filename = "pickle_vars/"+fname +".pkl"
    f = open(filename, 'rb')
    obj = pickle.load(f)
    f.close()
    return obj


# In[ ]:


writePickle(unique_words,"unique_words_training_dev")
writePickle(unique_tags,"unique_tags_training_dev")
writePickle(unique_POS,"unique_POS_training_dev")


# #### We create a class to extact the sentences formed by words present in the data files:

# In[ ]:


class SentenceGetter_m(object):    
    def __init__(self, data):
        self.n_sent = 1
        self.data = data
        self.empty = False
        agg_func = lambda s: [(w,p, t) for w, p, t in zip(s["lower_word"].values.tolist(),
                                                     s["pos"].values.tolist(),
                                                     s["tag"].values.tolist())]
        self.grouped = self.data.groupby(["file","spanNo"]).apply(agg_func)
        self.sentences = [s for s in self.grouped]
        
class SentenceGetter_test(object):    
    def __init__(self, data):
        self.n_sent = 1
        self.data = data
        self.empty = False
        agg_func = lambda s: [(w,p, t) for w, p, t in zip(s["lower_word"].values.tolist(),
                                                     s["pos"].values.tolist(),
                                                     s["tag"].values.tolist())]
        dict_func = lambda s: [(doc, span, word, pos, tag, index) for doc, span, word, pos, tag, index in zip(                                                     s["file"].values.tolist(),                                                    s["spanNo"].values.tolist(),                                                     s["lower_word"].values.tolist(),                                                     s["pos"].values.tolist(),                                                     s["tag"].values.tolist(),                                                    s["index"].values.tolist())]
        self.grouped = self.data.groupby(["file","spanNo"]).apply(agg_func)
        self.sentences = [s for s in self.grouped]
        self.dict = self.data.groupby(["file","spanNo"]).apply(dict_func)
        self.order = [s for s in self.dict]


# In[ ]:


# for training dataset
sentences_train = SentenceGetter_m(data_train).sentences
# for development dataset
sentences_dev = SentenceGetter_m(data_dev).sentences
# for the combined dataset
sentences_combined = SentenceGetter_m(data_combined).sentences


# The combined dataset will yield sentences in the following format

# In[ ]:


sentences_combined[0]


# With the following loop, we only extract the words

# In[ ]:


sentence_list = []
for sentence in sentences_combined:
    words =[]
    for word, pos, arg in sentence:
        words.append(word)
    sentence_list.append(words)
sentence_list[0].append('endpad') # we don't forget add 'endpad' to at least one sentence in the list
print(sentence_list[0:1])


# Record the max and min length of the datasets, to be used for padding later

# In[ ]:


max_len_train = max([len(s) for s in sentences_train])
min_len_train = min([len(s) for s in sentences_train])
max_len_dev = max([len(s) for s in sentences_dev])
min_len_dev = min([len(s) for s in sentences_dev])
print("The training set has a sentence length range of", (max_len_train, min_len_train))
print("The development set has a sentence length range of", (max_len_dev, min_len_dev))


# #### For working with the neural network structure, we need to generate dictionaries that map words and tags and POS tags to indices, and vice versa:

# In[ ]:


word2idx = {w: i for i, w in enumerate(unique_words)}
tag2idx = {t: i for i, t in enumerate(unique_tags)}
idx2word = {i: w for i, w in enumerate(unique_words)}
idx2tag = {i: t for i, t in enumerate(unique_tags)}
pos2idx = {p: i for i, p in enumerate(unique_POS)}
idx2pos = {i: p for i, p in enumerate(unique_POS)}


# So they will look like this:

# In[ ]:


print(tag2idx)
print(idx2tag)
print(word2idx['endpad'])


# Then we also record all these via pickle

# In[ ]:


writePickle(word2idx, "word2idx")
writePickle(tag2idx, "tag2idx")
writePickle(pos2idx, "pos2idx")
writePickle(idx2word, "idx2word")
writePickle(idx2tag, "idx2tag")
writePickle(idx2pos, "idx2pos")
writePickle(max_len_train, "max_len_train")
writePickle(max_len_dev, "max_len_dev")


# #### Now it is time to convert all the datasets into their indexed versions. So a sentence like [I think therefore i am] will be converted to something like [5 , 6, 7, 5, 13, endpad_index, endpad_index, ....]

# For training set:

# In[ ]:


# input for words
X_train = [[word2idx[w[0]] for w in s] for s in sentences_train]
X_train = pad_sequences(maxlen = max_len_train, sequences = X_train, padding = "pre", value = unique_word_count - 1)
# input for POS tags
P_train = [[pos2idx[w[1]] for w in s] for s in sentences_train]
P_train = pad_sequences(maxlen = max_len_train, sequences = P_train, padding = "pre", value = unique_POS_count - 1)
# output 
y_train = [[tag2idx[w[2]] for w in s] for s in sentences_train]
y_train = pad_sequences(maxlen = max_len_train, sequences = y_train, padding = "pre", value = tag2idx['endpad'])
print("The output will have a shape:", y_train.shape)
# convert the output to categorical format
y_train = [to_categorical(i, num_classes = unique_tag_count) for i in y_train]


# For development set:

# In[ ]:


# input for words
X_dev = [[word2idx[w[0]] for w in s] for s in sentences_dev]
X_dev = pad_sequences(maxlen = max_len_train, sequences = X_dev, padding = "pre", value = unique_word_count - 1)
# input for POS tags
P_dev = [[pos2idx[w[1]] for w in s] for s in sentences_dev]
P_dev = pad_sequences(maxlen = max_len_train, sequences = P_dev, padding = "pre", value = unique_POS_count - 1)
# output 
y_dev = [[tag2idx[w[2]] for w in s] for s in sentences_dev]
y_dev = pad_sequences(maxlen = max_len_train, sequences = y_dev, padding = "pre", value = tag2idx['endpad'])
print("The output will have a shape:", y_dev.shape)
# convert the output to categorical format
y_dev = [to_categorical(i, num_classes = unique_tag_count) for i in y_dev]


# In[ ]:


from pandas import DataFrame
broader_test_dataframe = DataFrame.from_records(test_data)
broader_test_dataframe.columns=['file','spanNo','word','pos','tag', 'index']
broader_test_dataframe['lower_word'] = broader_test_dataframe['word'].str.lower()
broader_test_dataframe.head()


# In[ ]:


sentences_test = SentenceGetter_test(broader_test_dataframe).order
X_test = []
X_index = [] #we'll use it to keep track of the index of relations and words
for sentence in sentences_test:
    index_test = []
    sentence_test = []
    for word in sentence:
        index_test.append((word[0], int(word[5])))
        if word[2] in word2idx.keys():
            sentence_test.append(word2idx[word[2]])
        else: # this case is for unknown words
            sentence_test.append(unique_word_count + 1)
    X_index.append(index_test)
    X_test.append(sentence_test)
X_test = pad_sequences(maxlen=max_len_train, sequences=X_test, padding = "pre", value=unique_word_count - 1)

print(X_index[0])
print(len(X_test))
print(len(np.unique(X_test, axis=0)))


# In[ ]:


P_test = [[pos2idx[w[3]] for w in s] for s in sentences_test]
P_test = pad_sequences(maxlen=max_len_train, sequences=P_test, padding = "pre", value=unique_POS_count - 1)

print(len(P_test))
print(len(np.unique(P_test, axis=0)))


# Also record all these via pickle

# In[ ]:


writePickle(X_train, "X_train_pre")
writePickle(P_train, "P_train_pre") # stands for the POS tags
writePickle(y_train, "y_train_pre")
writePickle(X_dev, "X_dev_pre")
writePickle(P_dev, "P_dev_pre") # stands for the POS tags
writePickle(y_dev, "y_dev_pre")
writePickle(X_test, "X_test_pre")
writePickle(P_test, "P_test_pre")
writePickle(X_index,"X_index_pre")


# #### CNN

# In[ ]:


unique_chars = set([w_i for w in unique_words for w_i in w]) # gets the unique characters
unique_char_count = len(unique_chars)


# In[ ]:


print("There are", unique_char_count, "characters in the combination of training and test sets")


# #### We need to generate dictionaries that map characters to indices, and vice versa, as we did previously for words and POS tags:

# In[ ]:


char2idx = {c: i + 2 for i, c in enumerate(unique_chars)} # leave the first two indices for 'unknown' and 'padding'
char2idx["UNK"] = 1
char2idx["PAD"] = 0


# Find out the token with the maximum number of characters, to be later used in padding:

# In[ ]:


max_len_char = max([len(w) for w in unique_words])
print("The longest token has a length of", max_len_char)


# For any given sentence list, generate a character indexed version of sentences

# In[ ]:


def char_list_generator(sentences, max_length):
    X_char = [] 
    for sentence in sentences:
        sent_seq = []
        for i in range(max_length):
            word_seq = []
            for j in range(max_len_char):
                try:
                    word_seq.append(char2idx.get(sentence[i][0][j]))
                except:
                    word_seq.append(char2idx.get("PAD"))
            sent_seq.append(word_seq)
        X_char.append(np.array(sent_seq))
    return X_char


# In[ ]:


char_list_train = char_list_generator(sentences_train, max_len_train)


# In[ ]:


char_list_dev = char_list_generator(sentences_dev, max_len_train)


# In[ ]:


print(char_list_dev[0]) # each row is a token


# In[ ]:


char_list_test = char_list_generator(sentences_test, max_len_train)


# In[ ]:


writePickle(unique_chars,"chars")
writePickle(char_list_train,"X_char_list_train")
writePickle(char_list_dev,"X_char_list_dev")
writePickle(char_list_test,"X_char_list_test")


# In[ ]:


'''
we also create the embedding matrix and save it via Pickle
'''
import gensim
import os
from gensim.models import KeyedVectors

emb_model = KeyedVectors.load_word2vec_format('glove.w2v', binary=False)


# In[ ]:


#own model
from gensim.models import Word2Vec
# train model
model_self = Word2Vec(sentence_list, size=300, min_count=1)


# In[ ]:


from numpy import zeros

embedding_matrix = np.zeros((len(word2idx) , 300))

for word, i in word2idx.items():
    if word in emb_model.wv.vocab:
        embedding_vector = emb_model[word]
        embedding_matrix[i] = embedding_vector
    elif word in model_self.wv.vocab:
        embedding_matrix[i] = model_self[word]
        
print(embedding_matrix.shape)
writePickle(embedding_matrix,"embedding_matrix_pre")


# In[ ]:




